﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public bool isPlaying; 
    public bool aiIsPlaying; // player choose to see AI playing
    public Player player;
    public AIPlayer aiPlayer;
    public Transform[] enemySpawners;
    public GameObject[] enemyTargets;

    public GameObject aiCollider; // AI object that will be enabled or disabled depends on player choice

    private void Start()
    {
        isPlaying = false;
        aiIsPlaying = false;
    }

    private void Awake()
    {
        #region Singleton
        if (instance==null)        
            instance = this;
        
        else if(instance != this)        
            Destroy(this);
        #endregion
    }

    //called when user hit "play" in main menu
    public void StartGame()
    {
        isPlaying = true;
        ScenarioManager.instance.MoveBridges(); //start moving bridges
        SpawnManager.instance.StartSpawningBarricade(); //start spawning barricades
        player.StartPlaying();  //player can play
    }

    //called when user hit "see ai" in main menu
    public void StartAIGame()
    {
        aiIsPlaying = true;
        aiPlayer.enabled = true;
        aiCollider.SetActive(true); //active ai collider
        StartGame();
    }
    
    public void PlayerDied()
    {
        GameOver();   
    }

    private void GameOver()
    {
        if(aiIsPlaying)
            aiCollider.SetActive(false); //disable ai collider
        aiIsPlaying = false;
        isPlaying = false;
        aiPlayer.enabled = false;
        StartCoroutine("GameOverCoroutine");
    }

    //wait a time to active main menu
    IEnumerator GameOverCoroutine()
    {
        yield return new WaitForSeconds(1.5f);
        Pooling.instance.DeactiveAllBarricade();
        MenuManager.instance.ActiveMainMenu();
    }

   
   
}
