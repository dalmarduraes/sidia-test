﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class GestureManager : MonoBehaviour {

    public Player player;
    private Vector3 startTouchPos;
    private Vector3 endTouchPos;
    private float dragMinDistance; // minimun distance (endTouch.x - startTouch.x) to be count as a swipe

    private void Start()
    {
        dragMinDistance = 100; 
    }

    void Update()
    {     
        //Player can move if the game is going go and it is not moving
        if (GameManager.instance.isPlaying && !GameManager.instance.aiIsPlaying)
        {
            #region Editor Move Keyboard
            if (Input.GetKeyDown(KeyCode.A))
                player.TryMove("Left");

            else if (Input.GetKeyDown(KeyCode.D))
                player.TryMove("Right");

            #endregion

            #region mobile touch
            //If there is a touch
            if (Input.touchCount > 0)
            {
                if (EventSystem.current.IsPointerOverGameObject() ||
                EventSystem.current.currentSelectedGameObject != null)
                {
                    return;
                }

                else
                {
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began)
                    {
                        //Get first touch position
                        startTouchPos = touch.position;
                    }
                    else if (touch.phase == TouchPhase.Ended)
                    {
                        //Get last touch position
                        endTouchPos = touch.position;
                        if (Mathf.Abs(endTouchPos.x - startTouchPos.x) > dragMinDistance)
                        {
                            //right
                            if ((endTouchPos.x > startTouchPos.x))
                                player.TryMove("Right");
                            //left
                            else
                                player.TryMove("Left");
                        }
                    }
                }
            }
            #endregion
        }    
    }
}
