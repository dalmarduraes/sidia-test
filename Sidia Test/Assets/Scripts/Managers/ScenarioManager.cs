﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioManager : MonoBehaviour {

    public static ScenarioManager instance;
    public Bridge lastBridge; //the bridge most far away from the player
    [SerializeField] private Bridge[] bridges;
    
    private void Awake()
    {
        #region Singleton
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(this);
        #endregion
    }

    //when bridge hit the trigger it will me moved to be the last bridge on scenario
    public void MoveBridgeToFront(Bridge b)
    {
        b.transform.position = lastBridge.spawner.transform.position;
        lastBridge = b;
    }

    //Call StartMoving in all bridges
    public void MoveBridges()
    {
        for(int i=0; i < bridges.Length; i++)
        {
            bridges[i].StartMoving();
        }
    }

}
