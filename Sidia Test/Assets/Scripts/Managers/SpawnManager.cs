﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnManager : MonoBehaviour {

    public static SpawnManager instance;   
    private Transform[] enemySpawners; // where the enemies will be spawn
    private GameObject[] enemyTargets;  //where the enemies will move towards
    [SerializeField]private float spawnRateInSec;  //spawn rate of enemies in seconds  
    
    private void Awake()
    {
        #region Singleton
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(this);
        #endregion        
    }

    private void Start()
    {
        //set the spawnRateInSec sec to 1
        if (spawnRateInSec <= 0)
        {
            spawnRateInSec = 1;
        }

        enemySpawners = GameManager.instance.enemySpawners;
        enemyTargets = GameManager.instance.enemyTargets;
    }

    public void StartSpawningBarricade()
    {
        InvokeRepeating("SpawnBarricade", 0.1f, spawnRateInSec); //spawn in give seconds
    }

    //get a barricade from pooling, active it and move to position
    private void SpawnBarricade()
    {
        if (GameManager.instance.isPlaying)
        {
            Barricade barricadeToSpawn = Pooling.instance.GetBarricadeReady();

            if (barricadeToSpawn != null)
            {
                int spawn = GetSpawner();                
                barricadeToSpawn.transform.position = enemySpawners[spawn].position;//move barricade to correct spawn             
                barricadeToSpawn.target = enemyTargets[spawn];   //set target to move towards
                barricadeToSpawn.ActiveDeactiveGameObj(true);
            }
        }
        else
            CancelInvoke();
    }

    //choose spawn where the enemy will be position
    private int GetSpawner()
    {
        Random.InitState((int)System.DateTime.Now.Ticks); //random seed
        int rand = Random.Range(0, 3);

        return rand;
    }
    
}
