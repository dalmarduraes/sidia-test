﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public static MenuManager instance;
    public GameObject pnlMenu;//mains menu panel

    private void Awake()
    {
        #region Singleton
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(this);
        #endregion
    }

    //User clicked on Play Button
    public void Play()
    {       
        pnlMenu.SetActive(false);
        GameManager.instance.StartGame();
    }

    //User clicked on SeeAI Button
    public void SeeAI()
    {       
        pnlMenu.SetActive(false);
        GameManager.instance.StartAIGame();
    }

    //User clicked on Exit Button
    public void ExitGame()
    {
        Application.Quit();
    }

    //Active main menu when user dies
    public void ActiveMainMenu()
    {
        pnlMenu.SetActive(true);
    }
}
