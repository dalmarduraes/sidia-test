﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeVertexColor : MonoBehaviour {

    //mesh component from meshfilter, will be changed color
    private Mesh mesh;    

    void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    void Start()
    {
        Vector3[] vertices = SetVertices();
        Color[] colors = RandomizeColors(vertices);
        SetColorToMesh(colors);
    }

    //set all object vertices
    private Vector3[] SetVertices()
    {
        //get mesh vertices
        Vector3[] v_Array = mesh.vertices;
        return v_Array;
    }

    private Color[] RandomizeColors(Vector3[] vertices)
    {       
        //create a color array to change array of each object vertice
        Color[] c_array = new Color[vertices.Length];

        for (int i = 0; i < vertices.Length; i++)
        {
            //randomize onde color
            c_array[i] = new Color(Random.Range(0.1f, 1), Random.Range(0.1f, 1), Random.Range(0.1f, 1), 1.0f);
        }

        return c_array;
    }

    private void SetColorToMesh(Color[] c)
    {
        // set the color to the mesh
        mesh.colors = c;
    }
}
