﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICollider : MonoBehaviour {

    public AIPlayer aiPlayer;
    
    //if collider with a enemy will tell AIPlayer
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            aiPlayer.ColliderFoundEnemy();
        }
    }
}
