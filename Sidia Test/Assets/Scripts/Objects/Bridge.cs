﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MovingScenario
{

    //obj in front of bridge that will be used to place another bridge
    public GameObject spawner;

    //collision
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Target")
        {
            ScenarioManager.instance.MoveBridgeToFront(this);
        }
    }
}
