﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingScenario : MonoBehaviour {

    public GameObject target;    
    public float speed;

    //Scenario Manager will call this method when bridges are ready to move
    public void StartMoving()
    {
        StartCoroutine("Move");
    }
    //Player move towards the position helper on its right or left. 
    private IEnumerator Move()
    {
        float moveRate = 0;
        while (GameManager.instance.isPlaying)
        {
            moveRate = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, moveRate);
            yield return new WaitForSeconds(0.01f);
        }

    }

    //collision
    protected virtual void OnTriggerEnter(Collider other) {}
}
