﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Barricade : MovingScenario
{
    //Start moving coroutine when enabled
    private void OnEnable()
    {
        StartMoving();
    }

    //Stop moving coroutine when disable
    private void OnDisable()
    {
        StopCoroutine("Move");
    }

    //Active or deactive obj
    public void ActiveDeactiveGameObj(bool value)
    {
        gameObject.SetActive(value);
    }

    //collision
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Target")
        {
            ActiveDeactiveGameObj(false);
        }
    }
}
