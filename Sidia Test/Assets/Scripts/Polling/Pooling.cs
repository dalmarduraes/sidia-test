﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooling : MonoBehaviour {

    public static Pooling instance;

    public PoolObjects poolObjects = new PoolObjects();

    private List<Barricade> barricades = new List<Barricade>();    

    private float timeToWaitBeforePooling;
    public float TimeToWaitBeforePooling
    {
        get { return timeToWaitBeforePooling; }
        set { timeToWaitBeforePooling = value; }
    }

    private void Awake()
    {
        #region Singleton
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(this);
        #endregion
    }

    private void Start()
    {
        Invoke("PoolBarricades", timeToWaitBeforePooling);
    }

    //pool "maxPooledBarricades" Barricades
    private void PoolBarricades()
    {        
        for (int i = 0; i < poolObjects.maxPooledBarricades; i++)
        {
            Barricade b = Instantiate(poolObjects.barricade);
            //Deactive Barricade
            b.ActiveDeactiveGameObj(false);
            b.transform.SetParent(transform);
            barricades.Add(b);
        }
    }

    //return a valid Barricade to use ingame
    public Barricade GetBarricadeReady()
    {
        for (int i = 0; i < barricades.Count; i++)
        {
            if(!barricades[i].gameObject.activeInHierarchy)                            
                return barricades[i];            
        }
        return null;
    }

    public void DeactiveAllBarricade()
    {
        for(int i =0; i < barricades.Count; i++)
        {
            barricades[i].gameObject.SetActive(false);
        }
    }

}

[System.Serializable]
public class PoolObjects
{
    public int maxPooledBarricades;
    [HideInInspector] public Barricade barricade;
    //Can add another info ex: differents gameobjects, particles
}
