﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlayer : MonoBehaviour {
    
    private Player player;    
    //how smart AI is
    [SerializeField]private int aiIntelligence;
    //will reduce intelligence a given max number
    private int maxIntelligenceModifier;

    private void OnEnable()
    {
        aiIntelligence = 100;
        maxIntelligenceModifier = 15;
        player = GetComponent<Player>();
    }
	
    //its called by AIcollider script
    public void ColliderFoundEnemy()
    {        
        TryAvoidEnemy();
    }

    //chance to avoid enemy
    private void TryAvoidEnemy()
    {        
        //if the AI is smart enough it can avoid the enemy
        int rand = Random.Range(0, 101);
        if(rand <= aiIntelligence)
        {
            AvoidEnemy();                  
        }
    }

    //chance to reduce intelligence
    private void TryReduceIntelligence()
    {       
        int rand = Randomize(0, 3); //75% chance to reduce intelligence
        if (rand <= 2)
        {
            aiIntelligence -= Randomize(0, maxIntelligenceModifier);
        }
        if (aiIntelligence < 0)
            aiIntelligence = 0;
    }

    //ai will change lane
    private void AvoidEnemy()
    {        
        //max lanes
        int maxPos = player.positionTarget.Length;

        int currentPos = player.CurrentPos;
        string direction = "";

        //if player is on left lane
        if (currentPos == 0)
            direction = "Right";      

        //if player is on right lane
        else if (currentPos == (maxPos-1))
            direction = "Left";

        //if player is on midle lane
        else
        {
            int rand = Random.Range(0, 2);
            if (rand == 0)
                direction = "Right";
            else
                direction = "Left";               
        }
        
        player.TryMove(direction);
        TryReduceIntelligence();     //some chance to reduce intelligence     
    }

    //Get randomValue of given min and max
    private int Randomize(int min, int max)
    {
        Random.InitState((int)System.DateTime.Now.Ticks); //random seed
        return Random.Range(min, max + 1);
    }

	
}
