﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    
    private Vector3 targetPos = new Vector3();  //pos to go when moves
    public ParticleSystem deathParticle;

    [SerializeField]  private float speed;
   
    public GameObject[] positionTarget; //Game Object to help with postion to go when user swipes left/right

    private int currentPos;
    public int CurrentPos { get { return currentPos; } }
    private bool moving;

    private BoxCollider boxCollider;

    private Animator animator;

    [SerializeField] private SkinnedMeshRenderer skinnedMeshRenderer;   //model mesh

    void Start()
    {
        if (speed <= 0)
            speed = 20;

        boxCollider = GetComponent<BoxCollider>();
        animator = GetComponent<Animator>();        
    }

    //When player hit start needs to configure these characteristcs
    public void StartPlaying()
    {
        boxCollider.enabled = true; //enable collider
        skinnedMeshRenderer.enabled = true; //enabled mesh   
        moving = false;
        currentPos = 1; //the currentPos is equals to the midle lane
        transform.position = positionTarget[currentPos].transform.position; // set position to midle lane
        StartRunAnimation();
    }

    //Will check if player can move and call the coroutine
    public void TryMove(string direction)
    {
        if (!moving && GameManager.instance.isPlaying)
        {
            targetPos = GetMoveTargetPos(direction);
            StartCoroutine("Move");
        }
    }

    //Will return the position thar player will move
    private Vector3 GetMoveTargetPos(string direction)
    {
        if(direction =="Left")
        {
            if (currentPos != 0) 
                currentPos--;            
        }

        else if(direction == "Right")
        {
            if (currentPos != (positionTarget.Length - 1)) 
                currentPos++;
        }

        return positionTarget[currentPos].transform.position;
    }

    //Player move towards the position helper on its right or left. 
    private IEnumerator Move()
    {
        bool reach = false;
        moving = true;
        while (!reach)
        {
            float moveRate = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetPos, moveRate);

            if (transform.position == targetPos)
                reach = true;

            yield return new WaitForSeconds(0.01f);            
        }
        moving = false;
    }
    
    private void StartRunAnimation()
    {
        animator.SetBool("isPlaying", true);
    }

    //Tell GameManager that player died and disable player.
    private void Die()
    {        
        GameManager.instance.PlayerDied();
        moving = false;    
        boxCollider.enabled = false;    //disable box collider
        animator.SetBool("isPlaying", false);   //start idle animation 
        skinnedMeshRenderer.enabled = false;        //remove mesh
        SpawnDeathParticle();
    }
   
    //instantiate death particles that will die by themselves
    private void SpawnDeathParticle()
    {
        Vector3 pos = transform.position;
        pos.y += 0.5f;
        GameObject particle = Instantiate(deathParticle.gameObject, pos, Quaternion.identity);
    }

    //collision
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Die();
        }
    }
}
