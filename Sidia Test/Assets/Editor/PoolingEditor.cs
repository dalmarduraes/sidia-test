﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Pooling))]
[CanEditMultipleObjects]
public class PollingEditor : Editor {

	
	public override void OnInspectorGUI ()
    {     
        base.OnInspectorGUI();       //show the Looping script variables on inspector    

        Pooling polling = (Pooling)target;

        GUILayout.Label("This script is a Editor");

        polling.TimeToWaitBeforePooling = EditorGUILayout.Slider("Time to wait before pooling: ", polling.TimeToWaitBeforePooling, 0, 1); //create float slider to TimeToWaitBeforePooling variable

        GUILayout.Label("Choose barricade on Prefbabs/Barricade");
        polling.poolObjects.barricade = EditorGUILayout.ObjectField("Barricade: ", polling.poolObjects.barricade, typeof( Barricade ), false) as Barricade; //the object chosen will be the barricade
    }
}
