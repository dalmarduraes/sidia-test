﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Profiling;
using System.Linq;


[CanEditMultipleObjects]
public class TaskMenuEditorWindow : EditorWindow {

    private List<Texture> allTextures = new List<Texture>();
    private List<Transform> selectedObjs = new List<Transform>(); //selected scene objects

    private Vector3 constantInterval = new Vector3 (0,0,0);    //user will set at editor menu

    private bool createCheckConsoleLabel = false;

    //Create menu on Unity's window tab
    [MenuItem("Window/Menu Editor - SIDIA")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<TaskMenuEditorWindow>("Menu Editor");
    }

    private void OnGUI()
    {
        #region Get all textures on projet and sort by size       

        GUILayout.Label("Click the button bellow to find all textures on project and then sort by size");

        //create button that will call find and sort textures method
        if (GUILayout.Button("Find and sort all textures"))
        {
            if (allTextures.Count > 0)
                allTextures.Clear();
            
            FillListWithAllTextures();
            SortTextureListByImageSize();
            //Creat label informing to check console
            createCheckConsoleLabel = true;
        }

        if (createCheckConsoleLabel)
            GUILayout.Label("Check Console for sorted list!");
        #endregion

        //Use a small slider as a line to separate tasks
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        #region Get all selected objects and position on a constant interval

        GUILayout.Label("Choose the interval, select objects on editor view and click the button bellow to position objects!");
        
        //user set constant interval at editor menu
        constantInterval = EditorGUILayout.Vector3Field("Choose constant interval:", constantInterval);

        if (GUILayout.Button("Position selected objs"))
        {
            if (!Selection.activeGameObject)            
                Debug.Log("Select a GameObject first");
            
            else
            {
                if(selectedObjs.Count>0)                
                    selectedObjs.Clear();

                //Fill "selectedObjs" List with all selected objs
                FillListWithAllSelectedObjs();      
                PositionSelectedObj();
            }
            
        }
        #endregion
    }    

    //Fill texture list
    private void FillListWithAllTextures()
    {
        foreach (Texture texture in Resources.FindObjectsOfTypeAll(typeof(Texture)))
        {
            allTextures.Add(texture);
        }

        Debug.Log("count: " + allTextures.Count);
    }

    //fill transform list with all selected objects
    private void FillListWithAllSelectedObjs()
    {
        foreach (Transform T in Selection.transforms)
        {
            selectedObjs.Add(T);
        }
    }

    //Position all selected objects on editor view on a constant interval
    private void PositionSelectedObj()
    {
        Vector3 constantIntervalAux = constantInterval; //an aux to help creat a constant intterval for each object
        if (selectedObjs.Count > 0)
        {
            foreach (Transform t in selectedObjs)
            {
                t.transform.position = constantIntervalAux;
                constantIntervalAux += constantInterval;
                Debug.Log("Obj: "+ t.name +" moved to: " + constantIntervalAux);
            }
        }
    }

    private void SortTextureListByImageSize()
    {
        //Using Linq's OrderBy method, I can order by the size of the texture file. Manual: https://docs.unity3d.com/ScriptReference/Profiling.Profiler.GetRuntimeMemorySizeLong.html
        if (allTextures.Count > 0)
        {
            allTextures = allTextures.OrderBy(texture => Profiler.GetRuntimeMemorySizeLong(texture)).ToList();
            
            for(int i=0;i< allTextures.Count;i++)
                Debug.Log("Texture named: " + allTextures[i].name + " is using " + Profiler.GetRuntimeMemorySizeLong(allTextures[i]) + " Bytes!");      
        }
        else
            Debug.Log("There is not textures on the project");



    }
}
