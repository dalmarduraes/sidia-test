﻿//Based on these shaders:  http://www.shaderslab.com/demo-41---animated-flag.html
Shader "Custom/FlagAnimation" {
    
	// Characterists of wave effect
	Properties
	{
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Speed ("Speed", Range(0, 10.0)) = 5
        _Frequency ("Frequency", Range(0, 2)) = 1
        _Amplitude ("Amplitude", Range(0, 5.0)) = 1
    }

    SubShader {
        Tags { "RenderType"="Opaque" }
        Cull off
       
        Pass
		{
 
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
 
            sampler2D _MainTex;
            float _Speed;
            float _Frequency;
            float _Amplitude;
 
            struct v2f 
			{
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };            
 
            v2f vert(appdata_base v)
            {
                v2f o;
				float time = _Time.y * 2;		

				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				v.vertex.y += sin((worldPos.x + time * _Speed) * _Frequency)* _Amplitude;
				
				o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                return o;
            }
 
            fixed4 frag(v2f i) : SV_Target
            {
                return tex2D(_MainTex, i.uv);
            }
 
            ENDCG
 
        }
    }
    FallBack "Diffuse"
}