Game made by Dalmar Dur�es

Unity version: 2018.2.3f1 (64-bit)

Assets used:
	-Simple Wooden Bridge: https://assetstore.unity.com/packages/3d/props/exterior/simple-wooden-bridge-819
	-RPG Character Lite: https://assetstore.unity.com/packages/3d/characters/rpg-character-lite-118141
	-Wooden Barricades: https://assetstore.unity.com/packages/3d/props/exterior/wooden-barricades-111243
	-Cartoon FX Free: https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-free-109565
	-Unity Particle Pack: https://assetstore.unity.com/packages/essentials/asset-packs/unity-particle-pack-73777
	-Medieval Stone Keep: https://assetstore.unity.com/packages/3d/environments/medieval-stone-keep-56596
	-WorldSkies Free: https://assetstore.unity.com/packages/2d/textures-materials/sky/worldskies-free-86517

Custom Shaders:
	-I used the Unity's standard shader
	-Transparent shader used in "Transparent" material. Used in Window_transparentobject on towers and barricades with window;
	-Self Iluminated shader used in "SelfIluminated" material. Used in "TopSelfIlumin" object.
	-Normal map shader used in "NormalMap" material. Used in Bridges objects.

Editor scripts:
	-"TaskMenuEditorWindow" Can be found on "Window/Menu Editor - SIDIA "
		-Use "Editor_Playground" Scene to position objects (Works on main scene but it is safer to use on Playground scene)
		-find and sort textures (show order on console). 
		-Position selected objects in given constant.

	-"PoolingEditor": Is the editor script for pooling script. Data selection: 
		-Game Object: "Barricade" can be add prefabs "Barricade" or "Barricade_WithWindow".
		-Float: "WaitTimeBeforePooling".
		-Class: "Pool objects" class. 

Vertex:
	- Vertex animator shader used to make wave effect on the flag.
	- Vertex Color script used in Barricades to give random color to vertices.
AI:
	-every time the AI avoid a enemy the intelligence has 75% chance to be reduced.

Render Order:
	Ui is in front of everything using the custom shader.